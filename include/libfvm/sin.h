/** @file include/sin.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <tuple>

#include "abs.h"
#include "copysign.h"
#include "sel.h"
#include "sincos.h"
#include "util.h"

namespace fvm {
    /// sin(x) for |x| < pi
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    sin_fast(const float x) noexcept
    {
        const auto sincos = impl::proto_sincos_fast(abs(x));
        const float sin = sel(std::get<2>(sincos) & 1, std::get<1>(sincos),
                              std::get<0>(sincos));
        return copysign(sin, x);
    }

    /// sin(x) for |x| < pi
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    sin_fast(const double x) noexcept
    {
        const auto sincos = impl::proto_sincos_fast(abs(x));
        const double sin = sel(std::get<2>(sincos) & 1, std::get<1>(sincos),
                               std::get<0>(sincos));
        return copysign(sin, x);
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            sin_fast(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = sin_fast(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
