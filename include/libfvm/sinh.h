/** @file include/sinh.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include "abs.h"
#include "exp.h"
#include "sel.h"
#include "util.h"

namespace fvm {
    /// return sinh(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    sinh(const double x) noexcept
    {
        // two strategies: small |x| (due to canellation of terms with
        // different signs) is done with a Padé approximant, large |x| is done
        // via the exponential, with a small tweak to avoid overflow
        constexpr double numer[] = {11511339840., 1640635920., 52785432.,
                                    479249.};
        constexpr double denom[] = {11511339840., -277920720., 3177720.,
                                    -18361.};
        const double x2 = x * x;
        const double sinh_smallx =
                x * impl::poly_eval(x2, numer) / impl::poly_eval(x2, denom);
        // need a bit of trickery to avoid overflow
        const double ex = exp(.5 * x);
        const double emx = 1. / ex;
        const double sinh_largex = (.5 * ex) * ex - (.5 * emx) * emx;
        return sel(abs(x) < .5, sinh_smallx, sinh_largex);
    }

    /// return sinh(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    sinh(const float x) noexcept
    {
        // two strategies: small |x| (due to canellation of terms with
        // different signs) is done with a Padé approximant, large |x| is done
        // via the exponential, with a small tweak to avoid overflow
        constexpr float numer[] = {166320.f, 22260.f, 551.f};
        constexpr float denom[] = {166320.f, -5460.f, 75.f};
        const float x2 = x * x;
        const float sinh_smallx =
                x * impl::poly_eval(x2, numer) / impl::poly_eval(x2, denom);
        // need a bit of trickery to avoid overflow
        const float ex = exp(.5f * x);
        const float emx = 1.f / ex;
        const float sinh_largex = (.5f * ex) * ex - (.5f * emx) * emx;
        return sel(abs(x) < 1.f, sinh_smallx, sinh_largex);
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            sinh(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = sinh(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
