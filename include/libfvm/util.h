/** @file include/util.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cstdint>
#include <climits>
#include <type_traits>

namespace fvm {
    namespace impl {
        /// factorial(n)
        [[gnu::const]] constexpr inline uint64_t
        factorial(uint64_t n) noexcept
        {
            uint64_t retVal = 1;
            while (n) retVal *= n--;
            return retVal;
        }

        /// 1 / factorial(n)
        template <typename T>
        [[gnu::const]] constexpr inline T ifactorial(unsigned n) noexcept
        {
            return T(1) / T(factorial(n));
        }

        /// helper to detect if T is indexable
        template <typename T,
                  typename = decltype(std::declval<const T&>()[0])>
        static constexpr std::true_type __detect_indexable(int) noexcept;
        /// helper to detect if T is indexable
        template <typename T>
        static constexpr std::false_type __detect_indexable(long) noexcept;
        /// is T indexable, i.e. does T's operator[] exist
        template <typename T>
        using is_indexable = decltype(__detect_indexable<T>(0));

        /// helper: detect T's underlying scalar type (if T is indexable)
        template <typename T, typename S = decltype(std::declval<const T&>()[0] + int8_t(0))>
        static constexpr S __detect_scalar(int) noexcept;
        /// helper: detect T's underlying scalar type (if T is not indexable)
        template <typename T>
        static constexpr T __detect_scalar(long) noexcept;

        /// figure out type of (underlying) scalar
        template <typename T>
        using scalar_type = decltype(__detect_scalar<T>(0));

        /// helper to detect if T is linear
        template <typename T, typename S = scalar_type<T>,
                  typename = decltype(S(2) * std::declval<const T&>() +
                                      std::declval<const T&>() -
                                      std::declval<const T&>() / S(2))>
        static constexpr std::true_type __detect_linear(int) noexcept;
        /// helper to detect if T is linear
        template <typename T>
        static constexpr std::false_type __detect_linear(long) noexcept;

        /// is T linear (supports +, -, and multiplication/division by scalar)
        template <typename T>
        using is_linear = decltype(__detect_linear<T>(0));

        /// is T vector-like
        template <typename T>
        using is_vector_like =
                std::integral_constant<bool, is_indexable<T>::value &&
                                                     is_linear<T>::value>;

        /// is T "small" vector (modelling a SIMD register or similar)
        template <typename T>
        using is_small_vector = std::integral_constant<
                bool, is_vector_like<T>::value &&
                              std::is_trivially_copyable<T>::value &&
                              std::is_trivially_destructible<T>::value &&
                              sizeof(T) * CHAR_BIT <= 512>;

        /// is T a (small) vector of floating point type
        template <typename T>
        using is_floating_point_vector = std::integral_constant<
                bool, is_small_vector<T>::value &&
                              std::is_floating_point<scalar_type<T>>::value>;

        /// is T a (small) vector of integral type
        template <typename T>
        using is_integer_vector = std::integral_constant<
                bool, is_small_vector<T>::value &&
                              std::is_integral<scalar_type<T>>::value>;

        /// enable overload if T is a floating point vector
        template <typename T>
        using enable_if_floating_point_vector_t =
                typename std::enable_if<is_floating_point_vector<T>::value,
                                        T>::type;

    } // namespace impl
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
