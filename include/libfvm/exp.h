/** @file include/exp.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cstdint>
#include <limits>

#include "bitcast.h"
#include "cheby_eval.h"
#include "copysign.h"
#include "fma.h"
#include "poly_eval.h"
#include "scalbn.h"
#include "sel.h"
#include "util.h"

namespace fvm {
    namespace impl {
        /// Padé approximation for (exp(x) - 1) / x for |x| <= 1
        [[gnu::const]] constexpr inline float
        _exm1x_pade(const float x) noexcept
        {
            // basically a Padé approximation, written down in a funny way
            // (exp(x) - 1) / x = 2 * R(x^2) / (Q(x^2) - x * R(x^2))
            // 4 FMAs, 2 mult., 1 div. => 11 FLOPS (6 mult., 4 add., 1 div.)
            constexpr float Q[] = {1680.f, 180.f, 1.f};
            constexpr float R[] = {840.f, 20.f};
            const float xx = x * x;
            const float q = poly_eval(xx, Q), r = poly_eval(xx, R);
            return 2.f * r / fma(-r, x, q);
        }

        /// Chebychev approximation for (exp(x) - 1) / x for |x| <= 1
        [[gnu::const]] constexpr inline float
        _exm1x_cheb(const float x) noexcept
        {
            // 14 FMAs, 1 mult. => 29 FLOPS (15 mult., 14 adds)
            // more FLOPS than the Padé version above (and less accurate), but
            // maybe useful when floating point division is very expensive
            constexpr float coeffs[] = {
                    1.0865211e+0, 5.3213167e-1, 8.7594271e-2, 1.0859013e-2,
                    1.0795593e-3, 8.9645386e-5, 6.4373016e-6, 4.7683716e-7,
            };
            return cheby_eval(x, coeffs);
        }

        /// Taylor approximation to (exp(x) - 1) / x for |x| <= 1
        [[gnu::const]] constexpr inline float
        _exm1x_taylor(const float x) noexcept
        {
            // 8 FMAs => 16 FLOPS (8 mult., 8 adds)
            constexpr float coeffs[] = {
                    ifactorial<float>(1), ifactorial<float>(2),
                    ifactorial<float>(3), ifactorial<float>(4),
                    ifactorial<float>(5), ifactorial<float>(6),
                    ifactorial<float>(7), ifactorial<float>(8),
                    ifactorial<float>(9)};
            return poly_eval(x, coeffs);
        }

        // fast floor
        [[gnu::const]] constexpr inline int32_t
        fast_floor(const float x) noexcept
        {
            return x;
        }
    } // namespace impl

    /// return exp(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    exp(const float x) noexcept
    {
        constexpr float maxargoflo = 88.02969f, iln2 = 1.442695e+00f,
                        ln2 = 6.933594e-01f, ln2c = -2.121944e-04f,
                        maxarg = 88.72284f, minarg = -87.33655f;
        // round correctly, but only if it doesn't overflow
        const auto whole = impl::fast_floor(
                x * iln2 +
                bit_cast<float>((-int32_t(x < maxargoflo)) &
                                bit_cast<int32_t>(copysign(.5f, x))));
        const float fwhole = whole;
        float y = fma(-ln2c, fwhole, fma(-ln2, fwhole, x));
        y = fma(y, impl::_exm1x_pade(y), 1.f);
        y *= impl::fscale(whole);
        y = sel(x > maxarg, std::numeric_limits<float>::infinity(), y);
        y = sel(x < minarg, 0.f, y);
        return y;
    }

    namespace impl {
        /// Chebychev approximation to (exp(x) - 1) / x for |x| <= 1
        [[gnu::const]] constexpr inline double
        _exm1x_cheb(const double x) noexcept
        {
            // 26 FMAs, 1 mult. => 53 FLOPS (27 mult., 26 adds)
            constexpr double coeffs[] = {
                    1.0865210970235899e+00, 5.3213175550401659e-01,
                    8.7594221922760562e-02, 1.0858923564136536e-02,
                    1.0794777745670725e-03, 8.9557320051003586e-05,
                    6.3748492606929830e-06, 3.9732585754848060e-07,
                    2.2023664003611998e-08, 1.0991036969443257e-09,
                    4.9879433916544258e-11, 2.0754509222342676e-12,
                    7.9714013168086240e-14, 2.8865798640254070e-15};
            return cheby_eval(x, coeffs);
        }

        /// Padé approximation to (exp(x) - 1) / x for |x| <= 1
        [[gnu::const]] constexpr inline double
        _exm1x_pade(const double x) noexcept
        {
            constexpr double Q[] = {17297280., 1995840., 25200., 56.};
            constexpr double R[] = {8648640., 277200., 1512., 1.};
            const double xx = x * x;
            const double r = poly_eval(xx, R), q = poly_eval(xx, Q);
            // 7 FMAs, 2 mult., 1 div. => 17 FLOPS (9 mult., 7 add. 1 div.)
            return 2. * r / fma(-r, x, q);
        }

        /// Taylor approximation to (exp(x) - 1) / x for |x| <= 1
        [[gnu::const]] constexpr inline double
        _exm1x_taylor(const double x) noexcept
        {
            // 15 FMAs, 30 FLOPS (15 mult., 15 adds)
            constexpr double coeffs[] = {
                    ifactorial<double>(1),  ifactorial<double>(2),
                    ifactorial<double>(3),  ifactorial<double>(4),
                    ifactorial<double>(5),  ifactorial<double>(6),
                    ifactorial<double>(7),  ifactorial<double>(8),
                    ifactorial<double>(9),  ifactorial<double>(10),
                    ifactorial<double>(11), ifactorial<double>(12),
                    ifactorial<double>(13), ifactorial<double>(14),
                    ifactorial<double>(15), ifactorial<double>(16)};
            return poly_eval(x, coeffs);
        }

        // fast floor
        [[gnu::const]] constexpr inline int32_t
        fast_floor(const double x) noexcept
        {
            return x;
        }
    } // namespace impl

    /// return exp(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    exp(const double x) noexcept
    {
        constexpr double maxargoflo = 709.08956571282405,
                         iln2 = 1.4426950408889634e+00,
                         ln2 = 6.9314718246459961e-01,
                         ln2c = -1.9046542999627671e-09,
                         maxarg = 709.78271289338397,
                         minarg = -708.39641853226408;
        // round correctly, but only if it doesn't overflow
        const auto _whole = impl::fast_floor(
                x * iln2 +
                bit_cast<double>((-int64_t(x < (maxargoflo))) &
                                 bit_cast<int64_t>(copysign(.5, x))));
        const double fwhole = _whole;
        const int64_t whole = _whole;
        double y = fma(-ln2c, fwhole, fma(-ln2, fwhole, x));
        y = fma(y, impl::_exm1x_pade(y), 1.);
        y *= impl::fscale(whole);
        y = sel(x > maxarg, std::numeric_limits<double>::infinity(), y);
        y = sel(x < minarg, 0., y);
        return y;
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            exp(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = exp(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
