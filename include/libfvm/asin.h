/** @file include/asin.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include "atan.h"
#include "invsqrt.h"
#include "util.h"

namespace fvm {
    /// return asin(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    asin(const float x) noexcept
    {
        return atan(x * invsqrt((1.f + x) * (1.f - x)));
    }

    /// return asin(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    asin(const double x) noexcept
    {
        return atan(x * invsqrt((1. + x) * (1. - x)));
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            asin(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = asin(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
