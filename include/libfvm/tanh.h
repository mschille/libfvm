/** @file include/tanh.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include "abs.h"
#include "copysign.h"
#include "fma.h"
#include "poly_eval.h"
#include "sel.h"
#include "util.h"

namespace fvm {
    /// return tanh(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    tanh(const double x) noexcept
    {
        // strategy for large arguments: exploit argument doubling/halving
        //
        // tanh(2x) = 2 tanh(x) / (1 + tanh^2(x))
        //
        // The idea is to use three "argument halving" steps to get |x| small
        // enough, use a Padé approximation for this "reduced" x, and then use
        // the argument doubling formula three times.
        constexpr double numer[] = {135135., 17325., 378., 1.};
        constexpr double denom[] = {135135., 62370., 3150., 28.};
        const double xx = x * 0.125;
        // Padé appromimation
        const double xx2 = xx * xx;
        double tanh = xx * impl::poly_eval(xx2, numer) /
                      impl::poly_eval(xx2, denom);
        // three argument doubling steps
        tanh = 2. * tanh / fma(tanh, tanh, 1.);
        tanh = 2. * tanh / fma(tanh, tanh, 1.);
        tanh = 2. * tanh / fma(tanh, tanh, 1.);
        // for very large |x| > 20, tanh(x) is x/|x| anyway (at least
        // to double precision)
        return sel(abs(x) > 1.83684002848385508067e1, copysign(1., x), tanh);
    }

    /// return tanh(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    tanh(const float x) noexcept
    {
        // same strategy as double version above, but even shorter Padé
        // approximation is sufficient for float
        constexpr float numer[] = {105.f, 10.f};
        constexpr float denom[] = {105.f, 45.f, 1.f};
        const float xx = x * 0.25f;
        // Padé appromimation
        const float xx2 = xx * xx;
        float tanh = xx * impl::poly_eval(xx2, numer) /
                     impl::poly_eval(xx2, denom);
        // two argument doubling steps
        tanh = 2.f * tanh / fma(tanh, tanh, 1.f);
        tanh = 2.f * tanh / fma(tanh, tanh, 1.f);
        return sel(abs(x) > 8.31776619f, copysign(1.f, x), tanh);
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            tanh(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = tanh(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
