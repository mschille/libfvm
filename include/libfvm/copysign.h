/** @file include/copysign.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <type_traits>

#include "abs.h"
#include "bitcast.h"
#include "sel.h"
#include "util.h"

namespace fvm {
    /// copysign for floating point types (x gets y's sign)
    template <typename T, typename = typename std::enable_if<
                                  std::is_floating_point<T>::value>::type>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline T copysign(const T x,
                                                          const T y) noexcept
    {
        using M = typename impl::mask_type<sizeof(T)>::type;
        constexpr M mask = (~M(0)) >> 1;
        return bit_cast<T>((bit_cast<M>(x) & mask) |
                           (bit_cast<M>(y) & ~mask));
    }

    /// copysign for integer types (x gets y's sign)
    template <typename T, typename = void,
              typename = typename std::enable_if<
                      std::is_integral<T>::value>::type>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline T copysign(const T x,
                                                          const T y) noexcept
    {
        const T ax = abs(x);
        return sel(y < T(0), -ax, ax);
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            copysign(T x, const T y) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = copysign(x[i], y[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
