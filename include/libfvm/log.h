/** @file include/log.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cstdint>
#include <limits>

#include "fma.h"
#include "frexp.h"
#include "poly_eval.h"
#include "sel.h"
#include "util.h"

namespace fvm {
    /// return log(x) for x > 0
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    log(const float x) noexcept
    {
        constexpr float ln2 = 6.933594e-01f, ln2c = -2.121944e-04f,
                        sqrthalf = 7.071068e-01f,
                        numer[] = {4620.f, 5430.f, 1360.f},
                        denom[] = {4620.f, 7740.f, 3690.f, 420.f, -9.f};
        int32_t e = 0;
        float y = impl::frexp(x, e);
        const float fcond = sel(y < sqrthalf, 1.f, 0.f);
        const float fe = e - fcond;
        y = fma(fcond + 1.f, y, -1.f);
        y = y * impl::poly_eval(y, numer) / impl::poly_eval(y, denom);
        y = fma(ln2, fe, fma(ln2c, fe, y));
        y = sel(x <= std::numeric_limits<float>::max(), y,
                std::numeric_limits<float>::infinity());
        y = sel(x > 0.f, y, -std::numeric_limits<float>::infinity());
        return sel(x >= 0.f, y, std::numeric_limits<float>::quiet_NaN());
    }

    /// return log(x) for x > 0
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    log(const double x) noexcept
    {
        constexpr double ln2 = 6.9314718246459961e-01,
                         ln2c = -1.9046542999627671e-09,
                         sqrthalf = 7.0710678118654746e-01,
                         numer[] = {327026700., 1053182130., 1317295980.,
                                    803847275., 245420560.,  33971616.,
                                    1534024.},
                         denom[] = {327026700.,  1216695480., 1816634820.,
                                    1388356200., 572822250.,  122921400.,
                                    11880540.,   337960.,     -1225.};
        int64_t e = 0;
        double y = impl::frexp(x, e);
        const bool cond = y < sqrthalf;
        const double fcond = sel(cond, 1., 0.);
        const double fe = e - fcond;
        y = fma(1. + fcond, y, -1.);
        y = y * impl::poly_eval(y, numer) / impl::poly_eval(y, denom);
        y = fma(ln2, fe, fma(ln2c, fe, y));
        y = sel(x <= std::numeric_limits<double>::max(), y,
                std::numeric_limits<double>::infinity());
        y = sel(x > 0., y, -std::numeric_limits<double>::infinity());
        return sel(x >= 0., y, std::numeric_limits<double>::quiet_NaN());
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            log(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = log(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
