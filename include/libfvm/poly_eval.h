/** @file include/poly_eval.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include "fma.h"
#include "util.h"

namespace fvm {
    namespace impl {
        /// evaluate polynomial (sum_{k=0}^{N} c_k x^k)
        template <typename T, typename S, std::size_t N>
        [[gnu::const]] constexpr inline typename std::enable_if<
                std::is_same<S, impl::scalar_type<T>>::value, T>::type
        poly_eval(const T x, const S (&coeffs)[N]) noexcept
        {
            // N-1 FMAs => 2*(N-1) FLOPS (N-1 mult., N-1 adds)
            T retVal = coeffs[N - 1];
            for (unsigned k = N - 1; k--;) retVal = fma(retVal, x, coeffs[k]);
            return retVal;
        }
    } // namespace impl
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
