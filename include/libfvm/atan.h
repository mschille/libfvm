/** @file include/atan.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include "abs.h"
#include "copysign.h"
#include "poly_eval.h"
#include "sel.h"
#include "util.h"

namespace fvm {
    namespace impl {
        /// return atan(x) for small |x|
        [[gnu::const]] constexpr inline float
        atan_smallx(const float x) noexcept
        {
            constexpr float numer[] = {3828825.f, 6831825.f, 3738735.f,
                                       638055.f, 16384.f},
                            denom[] = {3828825.f, 8108100.f, 5675670.f,
                                       1455300.f, 99225.f};
            const float xx = x * x;
            return x * impl::poly_eval(xx, numer) /
                   impl::poly_eval(xx, denom);
        }

        /// return atan(x) for small |x|
        [[gnu::const]] constexpr inline double
        atan_smallx(const double x) noexcept
        {
            constexpr double
                    numer[] = {65261681526586545.,  312513255440320935.,
                               634446395868969684., 711123375947135580.,
                               480172632833203310., 199960613704021170.,
                               50489265997350180.,  7288341707891244.,
                               531402124068585.,    14933359265055.,
                               68719476736.},
                    denom[] = {65261681526586545.,  334267149282516450.,
                               732816442657824525., 897865191004181400.,
                               673398893253136050., 318334022265118860.,
                               94131028089148050.,  16693187247336600.,
                               1622948760157725.,   72131056007010.,
                               940839860961.};
            const double xx = x * x;
            return x * impl::poly_eval(xx, numer) /
                   impl::poly_eval(xx, denom);
        }
    } // namespace impl

    /// return atan(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    atan(const float x) noexcept
    {
        constexpr float pi1_4 = 7.85398245e-1f;
        const auto xa = abs(x);
        const auto z = sel(xa < 1.f, xa, 1.f / xa);
        const auto y = impl::atan_smallx(z);
        return copysign(sel(xa < 1.f, y, pi1_4 - (y - pi1_4)), x);
    }

    /// return atan(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    atan(const double x) noexcept
    {
        constexpr double pi1_4 = 7.853981633974482790e-1;
        const auto xa = abs(x);
        const auto z = sel(xa < 1., xa, 1. / xa);
        const auto y = impl::atan_smallx(z);
        return copysign(sel(xa < 1., y, pi1_4 - (y - pi1_4)), x);
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            atan(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = atan(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:et:ft=cpp
