/** @file include/bitcast.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#undef LIBFVM_HAVE_BITCAST

#if __cplusplus > 202002L // std::bit_cast is only in C++20 and up

#include <bit>

// check that the C++ standard library supports it
#if defined(__cpp_lib_bit_cast)
namespace fvm {
    template <typename TO, typename FROM>
    using bit_cast = std::bit_cast<TO, FROM>;
} // namespace fvm

#define CONSTEXPR_IF_BITCAST constexpr
#define LIBFVM_HAVE_BITCAST

#endif // defined(__cpp_lib_bit_cast)
#endif // __cplusplus__

#if !defined(LIBFVM_HAVE_BITCAST)
// non-constexpr fallback implementation for systems without bit_cast support
#include <cstring>
#include <type_traits>

#if defined(__clang__) || defined(__GNUC__)
#define CONSTEXPR_IF_BITCAST constexpr
#else
#define CONSTEXPR_IF_BITCAST
#endif

namespace fvm {
    /// an (almost) conforming bit_cast implementation for C++11 and later
    template <typename TO, typename FROM,
              typename = typename std::enable_if<sizeof(TO) ==
                                                 sizeof(FROM)>::type,
              typename = typename std::enable_if<
                      std::is_trivially_copyable<FROM>::value>::type,
              typename = typename std::enable_if<
                      std::is_trivially_copyable<TO>::value>::type>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline TO
    bit_cast(const FROM& from) noexcept
    {
        typename std::aligned_storage<sizeof(TO), alignof(TO)>::type buf = {};
#if defined(__clang__) || defined(__GNUC__)
        __builtin_memcpy(&buf, &from, sizeof(TO));
#else
        std::memcpy(&buf, &from, sizeof(TO));
#endif
        return reinterpret_cast<TO&>(buf);
    }
} // namespace fvm


#endif // LIBFVM_HAVE_BITCAST

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
