/** @file include/hypot_fast.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include "abs.h"
#include "fma.h"
#include "minmax.h"
#include "util.h"

namespace fvm {
    /** @brief very fast approximation to hypothenuse, i.e. sqrt(x*x + y*y)
     *
     * This routine caculates a very fast approximation to the length of a
     * hypothenuse of a triangle given the length to the two cathetes. The
     * approximation is better than about 3.96 percent everywhere, and on
     * average about 2.4 percent off.
     */
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    hypot_fast(const float x, const float y) noexcept
    {
        const float xa = abs(x), ya = abs(y);
        return fma(0.96043387010341996525f, max(xa, ya),
                   0.39782473475931601382f * min(xa, ya));
    }

    /** @brief very fast approximation to hypothenuse, i.e. sqrt(x*x + y*y)
     *
     * This routine caculates a very fast approximation to the length of a
     * hypothenuse of a triangle given the length to the two cathetes. The
     * approximation is better than about 3.96 percent everywhere, and on
     * average about 2.4 percent off.
     */
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    hypot_fast(const double x, const double y) noexcept
    {
        const double xa = abs(x), ya = abs(y);
        return fma(0.96043387010341996525, max(xa, ya),
                   0.39782473475931601382 * min(xa, ya));
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            hypot_fast(T x, const T y) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = hypot_fast(x[i], y[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
