/** @file include/ldexp.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include "scalbn.h"

namespace fvm {
    /// return x * pow(2.f, exp) for exp in [-126, 127]
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    ldexp(const float x, const int exp) noexcept
    {
        return fvm::scalbn(x, exp);
    }

    /// return x * pow(2.f, exp) for exp in [-1022, 1023]
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    ldexp(const double x, const int exp) noexcept
    {
        return fvm::scalbn(x, exp);
    }

    template <typename T, typename I>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline typename std::enable_if<
            impl::is_floating_point_vector<T>::value &&
                    impl::is_integer_vector<I>::value &&
                    std::is_signed<impl::scalar_type<I>>::value &&
                    sizeof(T) == sizeof(I) &&
                    sizeof(impl::scalar_type<T>) ==
                            sizeof(impl::scalar_type<I>),
            T>::type
    ldexp(const T x, const I exp) noexcept
    {
        return fvm::scalbn(x, exp);
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
