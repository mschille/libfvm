/** @file include/fvm.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2021-01-28
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include "libfvm/fvm-config.h"

#include "libfvm/abs.h"
#include "libfvm/acos.h"
#include "libfvm/asin.h"
#include "libfvm/atan.h"
#include "libfvm/bitcast.h"
#include "libfvm/cbrt.h"
#include "libfvm/cheby_eval.h"
#include "libfvm/copysign.h"
#include "libfvm/cos.h"
#include "libfvm/cosh.h"
#include "libfvm/exp.h"
#include "libfvm/fifthrt.h"
#include "libfvm/fma.h"
#include "libfvm/frexp.h"
#include "libfvm/hypot_fast.h"
#include "libfvm/invcbrt.h"
#include "libfvm/invfifthrt.h"
#include "libfvm/invsqrt.h"
#include "libfvm/ldexp.h"
#include "libfvm/log.h"
#include "libfvm/minmax.h"
#include "libfvm/poly_eval.h"
#include "libfvm/scalbn.h"
#include "libfvm/sel.h"
#include "libfvm/sincos.h"
#include "libfvm/sin.h"
#include "libfvm/sinh.h"
#include "libfvm/sqrt.h"
#include "libfvm/tan.h"
#include "libfvm/tanh.h"
#include "libfvm/util.h"

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
