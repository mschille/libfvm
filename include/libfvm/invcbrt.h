/** @file include/cbrt.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cstdint>
#include <limits>

#include "abs.h"
#include "bitcast.h"
#include "copysign.h"
#include "fma.h"
#include "util.h"

namespace fvm {
    /// return 1 / cbrt(x) for nonzero x
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    invcbrt(const double x) noexcept
    {
        const double xa = abs(x);
        double r = bit_cast<double>(0x553f09fc6da44849 -
                                    (bit_cast<uint64_t>(xa) / 3));
        r = fma(0.333333333333333333, fma(-xa * r * r * r, r, r), r);
        r = fma(0.333333333333333333, fma(-xa * r * r * r, r, r), r);
        r = fma(0.333333333333333333, fma(-xa * r * r * r, r, r), r);
        r = fma(0.333333333333333333, fma(-xa * r * r * r, r, r), r);
        return copysign(r, x);
    }

    /// return 1 / cbrt(x) for nonzero x
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    invcbrt(const float x) noexcept
    {
        const float xa = abs(x);
        float r = bit_cast<float>(0x54a2fa8e - (bit_cast<uint32_t>(xa) / 3));
        r = fma(0.333333333f, fma(-xa * r * r * r, r, r), r);
        r = fma(0.333333333f, fma(-xa * r * r * r, r, r), r);
        r = fma(0.333333333f, fma(-xa * r * r * r, r, r), r);
        return copysign(r, x);
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            invcbrt(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = invcbrt(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
