/** @file include/cheby_eval.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include "fma.h"
#include "util.h"

namespace fvm {
    namespace impl {
        /// Chebychev approximation (for the interval |x| <= 1)
        template <typename T, typename S, std::size_t N>
        [[gnu::const]] constexpr inline typename std::enable_if<
                std::is_same<S, impl::scalar_type<T>>::value, T>::type
        cheby_eval(T x, const S (&coeffs)[N]) noexcept
        {
            static_assert(N, "N must be non-zero.");
            // assuming the last two calculations of the next Chebychev order
            // gets optimised away for the last two iterations of the loop,
            // one gets to:
            // 2*N-2 FMAs, 1 mult. => 4*N-1 FLOPS (2*N-1 mult., 2*N-2 adds)
            T retVal = 0, last = 1, curr = x;
            x *= T(2);
            for (const T& c : coeffs) {
                retVal = fma(c, last, retVal);
                const T next = fma(x, curr, -last);
                last = curr;
                curr = next;
            }
            return retVal;
        }
    } // namespace impl
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
