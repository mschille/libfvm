/** @file include/invsqrt.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cstdint>
#include <limits>

#include "abs.h"
#include "bitcast.h"
#include "sel.h"
#include "util.h"

namespace fvm {
    /// return 1 / sqrt(x) for x > 0
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    invsqrt(const double x) noexcept
    {
        const double xa = abs(x);
        double r = bit_cast<double>(0x5fe6eb3bfb58d152 -
                                    (bit_cast<uint64_t>(xa) >> 1));
        r = fma(0.5, fma(-xa * r * r, r, r), r);
        r = fma(0.5, fma(-xa * r * r, r, r), r);
        r = fma(0.5, fma(-xa * r * r, r, r), r);
        r = fma(0.5, fma(-xa * r * r, r, r), r);
        return sel(x >= 0, r, std::numeric_limits<double>::quiet_NaN());
    }

    /// return 1 / sqrt(x) for x > 0
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    invsqrt(const float x) noexcept
    {
        const float xa = abs(x);
        float r = bit_cast<float>(0x5f3759df - (bit_cast<uint32_t>(xa) >> 1));
        r = fma(0.5f, fma(-xa * r * r, r, r), r);
        r = fma(0.5f, fma(-xa * r * r, r, r), r);
        r = fma(0.5f, fma(-xa * r * r, r, r), r);
        return sel(x >= 0, r, std::numeric_limits<float>::quiet_NaN());
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            invsqrt(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = invsqrt(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
