/** @file include/cosh.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include "exp.h"
#include "util.h"

namespace fvm {
    /// return cosh(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    cosh(const double x) noexcept
    {
        // need a bit of trickery to avoid overflow
        const double ex = exp(.5 * x);
        const double emx = 1. / ex;
        return ((.5 * ex) * ex + (.5 * emx) * emx);
    }

    /// return cosh(x)
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    cosh(const float x) noexcept
    {
        // need a bit of trickery to avoid overflow
        const float ex = exp(.5f * x);
        const float emx = 1.f / ex;
        return ((.5f * ex) * ex + (.5f * emx) * emx);
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            cosh(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = cosh(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
