/** @file include/abs.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <type_traits>

#include "bitcast.h"
#include "sel.h"
#include "util.h"

namespace fvm {
    /// absolute value (for floating point types)
    template <typename T, typename = typename std::enable_if<
                                  std::is_floating_point<T>::value>::type>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline T abs(const T x) noexcept
    {
        using M = typename impl::mask_type<sizeof(T)>::type;
        constexpr M mask = (~M(0)) >> 1;
        return bit_cast<T>(bit_cast<M>(x) & mask);
    }
    /// absolute value (for integer types)
    template <typename T, typename = typename std::enable_if<
                                  std::is_integral<T>::value>::type>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline
            typename std::make_unsigned<T>::type
            abs(const T x) noexcept
    {
        return sel(x < 0, -x, x);
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            abs(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = abs(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
