/** @file include/cos.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <tuple>

#include "abs.h"
#include "copysign.h"
#include "sel.h"
#include "sincos.h"
#include "util.h"

namespace fvm {
    /// cos(x) for |x| < pi
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    cos_fast(const float x) noexcept
    {
        using namespace impl::sincos_float_constants;
        const float xa = abs(x);
        const auto sincos = impl::proto_sincos_fast(xa);
        const float cos = sel(std::get<2>(sincos) & 1, std::get<0>(sincos),
                              std::get<1>(sincos));
        return copysign(cos, pi1_2 - xa);
    }

    /// cos(x) for |x| < pi
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    cos_fast(const double x) noexcept
    {
        using namespace impl::sincos_double_constants;
        const double xa = abs(x);
        const auto sincos = impl::proto_sincos_fast(xa);
        const double cos = sel(std::get<2>(sincos) & 1, std::get<0>(sincos),
                               std::get<1>(sincos));
        return copysign(cos, pi1_2 - xa);
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline impl::
            enable_if_floating_point_vector_t<T>
            cos_fast(T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = cos_fast(x[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
