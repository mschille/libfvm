/** @file include/fma.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cmath>
#include <type_traits>

#include "util.h"

namespace fvm {
    /// use fused multiply-add if available
    [[gnu::const]] constexpr inline float fma(const float x, const float y,
                                              const float z) noexcept
    {
#if defined(FP_FAST_FMAF) // check if std::fma has fast hardware
                          // implementation
        return std::fma(x, y, z);
#else // defined(FP_FAST_FMA)
      // std::fma might be slow, so use a more pedestrian implementation
#if defined(__clang__)
#pragma STDC FP_CONTRACT ON // hint clang that using an FMA is okay here
#endif // defined(__clang__)
        return (x * y) + z;
#endif // defined(FP_FAST_FMA)
    }

    /// use fused multiply-add if available
    [[gnu::const]] constexpr inline double fma(const double x, const double y,
                                               const double z) noexcept
    {
#if defined(FP_FAST_FMA) // check if std::fma has fast hardware implementation
        return std::fma(x, y, z);
#else // defined(FP_FAST_FMA)
      // std::fma might be slow, so use a more pedestrian implementation
#if defined(__clang__)
#pragma STDC FP_CONTRACT ON // hint clang that using an FMA is okay here
#endif // defined(__clang__)
        return (x * y) + z;
#endif // defined(FP_FAST_FMA)
    }

    /// use fused multiply-add if available
    [[gnu::const]] constexpr inline long double
    fma(const long double x, const long double y,
        const long double z) noexcept
    {
#if defined(FP_FAST_FMAL) // check if std::fma has fast hardware
                          // implementation
        return std::fma(x, y, z);
#else // defined(FP_FAST_FMAL)
      // std::fma might be slow, so use a more pedestrian implementation
#if defined(__clang__)
#pragma STDC FP_CONTRACT ON // hint clang that using an FMA is okay here
#endif // defined(__clang__)
        return (x * y) + z;
#endif // defined(FP_FAST_FMAL)
    }

    /// fused multiply-add for mixed floating-point types
    template <typename U, typename V, typename W,
              typename T = typename std::common_type<
                      typename std::common_type<U, V>::type, W>::type>
    [[gnu::const]] constexpr inline
            typename std::enable_if<std::is_floating_point<T>::value, T>::type
            fma(const U x, const V y, const W z) noexcept
    {
        return fma(T(x), T(y), T(z));
    }

    /// fused multiply-add for (potentially mixed) integer types (i.e. a MAC)
    template <typename U, typename V, typename W,
              typename T = typename std::common_type<
                      typename std::common_type<U, V>::type, W>::type>
    [[gnu::const]] constexpr inline
            typename std::enable_if<std::is_integral<T>::value, T>::type
            fma(const U x, const V y, const W z) noexcept
    {
#if defined(__clang__)
#pragma STDC FP_CONTRACT ON // hint clang that using an FMA is okay here
#endif // defined(__clang__)
        return (T(x) * T(y)) + T(z);
    }

    template <typename T>
    [[gnu::const]] constexpr inline impl::enable_if_floating_point_vector_t<T>
    fma(T x, T y, T z) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = fma(x[i], y[i], z[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
