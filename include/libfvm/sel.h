/** @file include/sel.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cstdint>

#include "bitcast.h"

namespace fvm {
    /// implementation details
    namespace impl {
        /// helper class which gives suitable integer type of given size
        template <std::size_t SZ>
        struct mask_type;
        template <>
        struct mask_type<1> {
            using type = uint8_t;
        };
        template <>
        struct mask_type<2> {
            using type = uint16_t;
        };
        template <>
        struct mask_type<4> {
            using type = uint32_t;
        };
        template <>
        struct mask_type<8> {
            using type = uint64_t;
        };
    } // namespace impl

    /// select: essentially a branchless if, i.e. cond ? a : b
    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline T sel(bool cond, const T& a,
                                                     const T& b) noexcept
    {
        static_assert(1 <= sizeof(T) && sizeof(T) <= 8,
                      "the size of a T must be between 1 and 8 bytes");
        static_assert(0 == (sizeof(T) & (sizeof(T) - 1)),
                      "the size of a T must be a power of 2");
        using mask_t = typename impl::mask_type<sizeof(T)>::type;
        const mask_t mask(-mask_t(cond));
        return bit_cast<T>(
                static_cast<mask_t>((mask & bit_cast<mask_t>(a)) |
                                    (~mask & bit_cast<mask_t>(b))));
    }

    // [FIXME] need a suitable extension of sel to vector types - MS
    // - needs to take a bool or a suitable mask as condition
    // - may have to widen (broadcast) a scalar to all elements of a vector
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
