/** @file include/frexp.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cstdint>

#include "bitcast.h"
#include "util.h"

namespace fvm {
    namespace impl {
        /// convert float into fractional part and exponent
        [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
        frexp(const float x, int32_t& exp) noexcept
        {
            constexpr int expbits = 23;
            constexpr int32_t expmask = int32_t(255) << expbits;
            constexpr int32_t expbias = 127;
            exp = ((bit_cast<int32_t>(x) & expmask) >> expbits) -
                  (expbias - 1);
            return bit_cast<float>((bit_cast<int32_t>(x) & ~expmask) |
                                   (((expbias - 1) & 255) << expbits));
        }

        /// convert float into fractional part and exponent
        [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
        frexp(const double x, int64_t& exp) noexcept
        {
            constexpr int expbits = 52;
            constexpr int64_t expmask = int64_t(2047) << expbits;
            constexpr int64_t expbias = 1023;
            exp = ((bit_cast<int64_t>(x) & expmask) >> expbits) -
                  (expbias - 1);
            return bit_cast<double>((bit_cast<int64_t>(x) & ~expmask) |
                                    (((expbias - 1) & 2047) << expbits));
        }
    } // namespace impl

    /// return fractional part and set exponent
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float frexp(float x,
                                                           int& exp) noexcept
    {
        int32_t e = 0;
        x = impl::frexp(x, e);
        exp = e;
        return x;
    }
    /// return fractional part and set exponent
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float frexp(float x,
                                                           int* exp) noexcept
    {
        return frexp(x, *exp);
    }

    /// return fractional part and set exponent
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double frexp(double x,
                                                            int& exp) noexcept
    {
        int64_t e = 0;
        x = impl::frexp(x, e);
        exp = e;
        return x;
    }
    /// return fractional part and set exponent
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double frexp(double x,
                                                            int* exp) noexcept
    {
        return frexp(x, *exp);
    }

    template <typename T, typename I>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline typename std::enable_if<
            impl::is_floating_point_vector<T>::value &&
                    impl::is_integer_vector<I>::value &&
                    sizeof(T) == sizeof(I) &&
                    sizeof(impl::scalar_type<T>) ==
                            sizeof(impl::scalar_type<I>),
            T>::type
    frexp(T x, I& exp) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] = frexp(x[i], exp[i]);
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
