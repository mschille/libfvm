/** @file include/scalbn.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cstdint>

#include "bitcast.h"
#include "minmax.h"
#include "util.h"

namespace fvm {
    namespace impl {
        /// return pow(2.f, exp) for exp in [-126, 127]
        [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
        fscale(const int32_t exp) noexcept
        {
            return bit_cast<float>(uint32_t(exp + 127) << 23);
        }

        /// return pow(2.f, exp) for exp in [-1022, 1023]
        [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
        fscale(const int64_t exp) noexcept
        {
            return bit_cast<double>(uint64_t(exp + 1023) << 52);
        }
    } // namespace impl

    /// return x * pow(2.f, exp) for exp in [-126, 127]
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline float
    scalbn(const float x, const int exp) noexcept
    {
        return x * impl::fscale(min(int32_t(128), max(int32_t(-127), exp)));
    }

    /// return x * pow(2.f, exp) for exp in [-1022, 1023]
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline double
    scalbn(const double x, const int exp) noexcept
    {
        return x * impl::fscale(min(int64_t(1024),
                                    max(int64_t(-1023), int64_t(exp))));
    }

    template <typename T, typename I>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline typename std::enable_if<
            impl::is_floating_point_vector<T>::value &&
                    impl::is_integer_vector<I>::value &&
                    std::is_signed<impl::scalar_type<I>>::value &&
                    sizeof(T) == sizeof(I) &&
                    sizeof(impl::scalar_type<T>) ==
                            sizeof(impl::scalar_type<I>),
            T>::type
    scalbn(T x, const I exp) noexcept
    {
        using SI = impl::scalar_type<I>;
        constexpr auto minexp = (sizeof(SI) == sizeof(int32_t)) ? -127 : -1023;
        constexpr auto maxexp = (sizeof(SI) == sizeof(int32_t)) ? 128 : 1024;
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        for (auto i = 0; sz != i; ++i)
            x[i] *= impl::fscale(min(SI(maxexp), max(SI(minexp), exp[i])));
        return x;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
