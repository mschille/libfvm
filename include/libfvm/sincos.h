/** @file include/sincos.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cstdint>
#include <tuple>

#include "abs.h"
#include "copysign.h"
#include "poly_eval.h"
#include "sel.h"
#include "util.h"

namespace fvm {
    namespace impl {
        /// sin(x) for |x| < pi/4
        [[gnu::const]] constexpr inline float
        sin_smallx(const float x) noexcept
        {
            constexpr float numer[] = {166320.f, -22260.f, 551.f},
                            denom[] = {166320.f, 5460.f, 75.f};
            const float xx = x * x;
            return x * poly_eval(xx, numer) / poly_eval(xx, denom);
        }

        /// sin(x) for |x| < pi/4
        [[gnu::const]] constexpr inline double
        sin_smallx(const double x) noexcept
        {
            constexpr double numer[] = {124345562140800., -17478564143040.,
                                        538531796880., -4469552712.},
                             denom[] = {124345562140800., 3245696213760.,
                                        43268148000., 366075360., 1768969.};
            const double xx = x * x;
            return x * poly_eval(xx, numer) / poly_eval(xx, denom);
        }

        /// cos(x) for |x| < pi/4
        [[gnu::const]] constexpr inline float
        cos_smallx(const float x) noexcept
        {
            constexpr float numer[] = {15120.f, -6900.f, 313.f},
                            denom[] = {15120.f, 660.f, 13.f};
            const float xx = x * x;
            return poly_eval(xx, numer) / poly_eval(xx, denom);
        }

        /// cos(x) for |x| < pi/4
        [[gnu::const]] constexpr inline double
        cos_smallx(const double x) noexcept
        {
            constexpr double numer[] = {23594700729600., -11275015752000.,
                                        727718024880., -13853547000.,
                                        80737373.},
                             denom[] = {23594700729600., 522334612800.,
                                        5772800880., 39328920., 147173.};
            const double xx = x * x;
            return poly_eval(xx, numer) / poly_eval(xx, denom);
        }

        /// float constants for sin/cos calculations
        namespace sincos_float_constants {
            constexpr float pi1_2 = 1.570796251f, ipi1_2 = 6.366198063e-1f,
                            pi1_2c = 1.5703125f, pi1_2cc = 4.839897156e-4f,
                            pi1_2ccc = -1.629206849e-7f;
        } // namespace sincos_float_constants

        /// double constants for sin/cos calculations
        namespace sincos_double_constants {
            constexpr double pi1_2 = 1.570796326794896558,
                             ipi1_2 = 6.366197723675814935e-1,
                             pi1_2c = 1.570796340703964233,
                             pi1_2cc = -1.390906767539945577e-8,
                             pi1_2ccc = 6.123233995736767268e-17;
        } // namespace sincos_double_constants

        /// remainder and quotient of x > 0 divided by pi / 2
        [[gnu::const]] CONSTEXPR_IF_BITCAST inline std::tuple<float, int32_t>
        remquopi2(const float x) noexcept
        {
            // this is a bit more complex, as we have to avoid losing
            // precision; we do that by saving pi / 2 in chunks which only use
            // the first half of the mantissa bits (except for the last one).
            // More operations are needed to fully subtract quotient * pi / 2
            // from x, but there are enough guard bits to avoid excessive
            // cancellations.
            using namespace sincos_float_constants;
            const int32_t quo = fma(ipi1_2, x, 0.5f);
            const float fquo = quo;
            const float rem = fma(fquo, -pi1_2ccc,
                                  fma(fquo, -pi1_2cc, fma(fquo, -pi1_2c, x)));
            return std::make_tuple(rem, quo);
        }

        /// remainder and quotient of x > 0 divided by pi / 2
        [[gnu::const]] CONSTEXPR_IF_BITCAST inline std::tuple<double, int64_t>
        remquopi2(const double x) noexcept
        {
            // this is a bit more complex, as we have to avoid losing
            // precision; we do that by saving pi / 2 in chunks which only use
            // the first half of the mantissa bits (except for the last one).
            // More operations are needed to fully subtract quotient * pi / 2
            // from x, but there are enough guard bits to avoid excessive
            // cancellations.
            using namespace sincos_double_constants;
            const int64_t quo = fma(ipi1_2, x, 0.5);
            const double fquo = quo;
            const double rem =
                    fma(fquo, -pi1_2ccc,
                        fma(fquo, -pi1_2cc, fma(fquo, -pi1_2c, x)));
            return std::make_tuple(rem, quo);
        }

        /// "rump" of the sin/cos calculation for small positive arguments
        [[gnu::const]] CONSTEXPR_IF_BITCAST inline std::tuple<float, float,
                                                              int32_t>
        proto_sincos_fast(const float x) noexcept
        {
            using namespace sincos_float_constants;
            auto remquo = remquopi2(x);
            std::get<0>(remquo) = abs(std::get<0>(remquo));
            return std::make_tuple(impl::sin_smallx(std::get<0>(remquo)),
                                   impl::cos_smallx(std::get<0>(remquo)),
                                   std::get<1>(remquo));
        }

        /// "rump" of the sin/cos calculation for small positive arguments
        [[gnu::const]] CONSTEXPR_IF_BITCAST inline std::tuple<double, double,
                                                              int64_t>
        proto_sincos_fast(const double x) noexcept
        {
            using namespace sincos_double_constants;
            auto remquo = remquopi2(x);
            std::get<0>(remquo) = abs(std::get<0>(remquo));
            return std::make_tuple(impl::sin_smallx(std::get<0>(remquo)),
                                   impl::cos_smallx(std::get<0>(remquo)),
                                   std::get<1>(remquo));
        }
    } // namespace impl

    /// return a tuple (sin(x), cos(x)) for |x| < pi
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline std::tuple<float, float>
    sincos_fast(const float x) noexcept
    {
        using namespace impl::sincos_float_constants;
        const float xa = abs(x);
        const auto sincos = impl::proto_sincos_fast(xa);
        const float sin = sel(std::get<2>(sincos) & 1, std::get<1>(sincos),
                              std::get<0>(sincos));
        const float cos = sel(std::get<2>(sincos) & 1, std::get<0>(sincos),
                              std::get<1>(sincos));
        return std::make_tuple(copysign(sin, x), copysign(cos, pi1_2 - xa));
    }

    /// return a tuple (sin(x), cos(x)) for |x| < pi
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline std::tuple<double, double>
    sincos_fast(const double x) noexcept
    {
        using namespace impl::sincos_double_constants;
        const double xa = abs(x);
        const auto sincos = impl::proto_sincos_fast(xa);
        const double sin = sel(std::get<2>(sincos) & 1, std::get<1>(sincos),
                               std::get<0>(sincos));
        const double cos = sel(std::get<2>(sincos) & 1, std::get<0>(sincos),
                               std::get<1>(sincos));
        return std::make_tuple(copysign(sin, x), copysign(cos, pi1_2 - xa));
    }

    template <typename T>
    [[gnu::const]] CONSTEXPR_IF_BITCAST inline
            typename std::enable_if<impl::is_floating_point_vector<T>::value,
                                    std::tuple<T, T>>::type
            sincos_fast(const T x) noexcept
    {
        constexpr auto sz = sizeof(T) / sizeof(impl::scalar_type<T>);
        std::tuple<T, T> retVal;
        for (auto i = 0; sz != i; ++i)
            std::tie(std::get<0>(retVal)[i], std::get<1>(retVal)[i]) =
                    sincos_fast(x[i]);
        return retVal;
    }
} // namespace fvm

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
