# libfvm

`libfvm` is a small header-only C++ library for (some of) the math functions
that are commonly found in a standard C math library. The routines in `libfvm`
are designed to be fast and easily (auto-)vectorizable. Their accuracy is
typically almost all but the last 2-4 bits of the mantissa of the floating
point numbers. It features separate overloads for single and double precision
floating point numbers (and assumes them to be in IEEE 754 binary format
matching the endianness of the target CPU). It requires C++14 (and C++20 if
you want most functions to be constexpr).

## Aims

`libfvm` aims to be a generic and architecture-neutral library that gives you a
reasonable starting point for your development. If your hardware has special
capabilities (e.g. an instruction to compute square roots in hardware), you
will likely want to replace or augment some routines to take advantage of that
on your target. `libfvm` aims to provide reasonable fallback implementations
which should help in porting. Once the port works, you can always make it
faster... If your specific application does not require the full accuracy
provided by `libfvm`, it should also be easy to take the idea of the
implementation, and quickly replace an approximation in it by a lower quality
one, thereby gaining (sometimes massive amounts of) speed.

## Supported functions

- `abs`, `copysign`, `min`, `max`, `sel`
- `fma` (opportunistic FMA - if hardware supports fast FMA, use it, fall back
  to multiply and add otherwise)
- `frexp`, `ldexp`, `scalbn`
- `sqrt`, `cbrt`, `fifthrt`
- `invsqrt`, `invcbrt`, `invfifthrt`
- `exp`, `log`
- `cosh`, `sinh`, `tanh`
- `cos_fast`, `sin_fast`, `tan_fast`, `sincos_fast` (for arguments |x| < pi)
- `acos`, `asin`, `atan`
- evaluation of polynomials and Chebychev series

## Compiling and unit tests

`libfvm` is a header-only library, so it's enough for make your project to pick
up `libfvm`'s `include` directory. All functions are in namespace `fvm::`. At
the time of this writing, clang seems to produce good code with something along
the lines of `-O2 -ftree-vectorize -march=native`, whereas gcc needs more
agfgressive optimisation setting along the lines of `-O3 -ftree-vectorize
-march=native` to produce good code.

The library comes with a bunch of unit tests, and a simple CMake-based build
system. To compile it, use these commands in side the libfvm top-level
directory (adjust compiler below to suit your preferences):

``` shell
mkdir build
cd build
cmake -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ ..
make
```

This should create a `build` directory with the unit test binaries in. You can
run them manually, or by typing `make test` in the build subdirectory.

## Speed

There is no way to predict the speedup for a particular architecture, compiler
or application - you will have to benchmark. Depending on your calling pattern
(working on long arrays or SIMD registers typically is fastest) and the
capabilities of your CPU/FPU (SIMD width, hardware support for special
functions like sqrt, exp, log, ...), the results will vary. Run
`build/tests/bench` to get an idea of how `libfvm` compares to the routines
that come with your system/compiler.

A good rule of thumb is that you can expect a speedup of up to the width of an
SIMD register (in the floating point type of your choice), provided that there
is no hardware to help with that operation (e.g. square root instruction) that
is used by the standard library, and that the two implementations are about
equally clever (e.g. calculating fifth roots will be much faster in `libfvm`
because it uses a specialised algorithm). On my laptop with its 256 bit SIMD
register width, I would expect a speedup of around 8 for single precision (32
bit) floating point numbers, and half that for double precision (64 bit).

## Accuracy

Typically, you can trust all but the last four bits of the mantissa, although
that is not a hard rule, and there are no unit tests to exhausively verify this
(for double precision, that is currently not computationally feasible, and it
is too slow for single precision). There is a unit test `build/tests/accuracy`
which verifies the claim made above statistically over the entire range on
which a function is defined, taking care to sample also very small/large
values.

## TODO

Find better approximations, add missing functions, clean up source code,
improve documentation, provide overloads for types used in SIMD libraries...

Basically, if you need something, have a good idea, or even some code, please
get in contact.

