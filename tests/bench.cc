/** @file tests/bench.cc
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#include <array>
#include <chrono>
#include <cmath>
#include <cstdio>
#include <random>
#include <tuple>
#include <type_traits>

#include "libfvm/acos.h"
#include "libfvm/asin.h"
#include "libfvm/atan.h"
#include "libfvm/cbrt.h"
#include "libfvm/cos.h"
#include "libfvm/cosh.h"
#include "libfvm/exp.h"
#include "libfvm/fifthrt.h"
#include "libfvm/invcbrt.h"
#include "libfvm/invfifthrt.h"
#include "libfvm/invsqrt.h"
#include "libfvm/log.h"
#include "libfvm/sin.h"
#include "libfvm/sinh.h"
#include "libfvm/sqrt.h"
#include "libfvm/tan.h"
#include "libfvm/tanh.h"

static std::mt19937_64 rng;

template <typename T>
T uniformrng(const T min, const T max) noexcept
{
    const double f = double(rng()) / double(rng.max());
    return T(f * min + (1. - f) * max);
}

template <typename T, typename FN>
[[gnu::noinline, gnu::hot, gnu::flatten]] void
___bench(const T* first, const T* last, T* dest, FN&& fn) noexcept
{
    for (; last != first; ++first, ++dest) *dest = fn(*first);
}

template <typename T, typename FN>
double __bench(const T* first, const T* last, T* dest, FN&& fn,
               double tnop = 0) noexcept
{
    constexpr unsigned ntimes = 128;
    double dtmin = std::numeric_limits<float>::max();
    for (unsigned i = 0; ntimes != i; ++i) {
        const auto t1 = std::chrono::high_resolution_clock::now();
        ___bench(first, last, dest, std::forward<FN>(fn));
        const auto t2 = std::chrono::high_resolution_clock::now();
        const double dt =
                std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1)
                        .count();
        if (dt < dtmin) dtmin = dt;
    }
    dtmin /= std::distance(first, last);
    dtmin -= tnop;
    return dtmin;
}

template <typename T, typename FN>
double _bench(FN&& fn, double tnop = 0)
{
    constexpr unsigned arrsz = 1024;
    alignas(64) std::array<T, arrsz> arr, dest;
    for (auto& el : arr) el = uniformrng(std::numeric_limits<T>::min(), T(1));
    const double dt = __bench(arr.data(), arr.data() + arr.size(),
                              dest.data(), std::forward<FN>(fn), tnop);
    return dt;
}

// reference function helpers for the cases where the function in std:: does
// not quite do it
namespace ref {
    template <typename T>
    inline T fifthrt(const T x) noexcept
    {
        return std::pow(x, T(1) / T(5));
    }
    template <typename T>
    inline T invsqrt(const T x) noexcept
    {
        return T(1) / std::sqrt(x);
    }
    template <typename T>
    inline T invcbrt(const T x) noexcept
    {
        return T(1) / std::cbrt(x);
    }
    template <typename T>
    inline T invfifthrt(const T x) noexcept
    {
        return std::pow(x, T(-1) / T(5));
    }
    template <typename T>
    inline T nop(const T x) noexcept
    {
        return x;
    }
} // namespace ref

template <typename FN, typename FNREF>
std::tuple<double, double, double, double>
bench(const char* name, FN&& fn, FNREF&& fnref, double tnopflt = 0,
      double tnopdbl = 0)
{
    const auto tfnflt = _bench<float>(std::forward<FN>(fn), tnopflt);
    const auto tfnrefflt = _bench<float>(std::forward<FNREF>(fnref), tnopflt);
    const auto tfndbl = _bench<double>(std::forward<FN>(fn), tnopdbl);
    const auto tfnrefdbl =
            _bench<double>(std::forward<FNREF>(fnref), tnopdbl);
    std::printf("%20s: float %6.2f ns ref %6.2f ns speedup %5.2f double "
                "%6.2f ns ref %6.2f ns speedup %5.2f\n",
                name, tfnflt, tfnrefflt, tfnrefflt / tfnflt, tfndbl,
                tfnrefdbl, tfnrefdbl / tfndbl);
    return std::make_tuple(tfnflt, tfnrefflt, tfndbl, tfnrefdbl);
}

void bench()
{
    const auto tnop = bench(
            "nop", [](auto x) noexcept { return x; },
            [](auto x) noexcept { return x; });
    const auto& tnopflt = std::get<0>(tnop);
    const auto& tnopdbl = std::get<2>(tnop);
    bench(
            "exp", [](auto x) noexcept { return fvm::exp(x); },
            [](auto x) noexcept { return std::exp(x); }, tnopflt, tnopdbl);
    bench(
            "log", [](auto x) noexcept { return fvm::log(x); },
            [](auto x) noexcept { return std::log(x); }, tnopflt, tnopdbl);
    bench(
            "cosh", [](auto x) noexcept { return fvm::cosh(x); },
            [](auto x) noexcept { return std::cosh(x); }, tnopflt, tnopdbl);
    bench(
            "sinh", [](auto x) noexcept { return fvm::sinh(x); },
            [](auto x) noexcept { return std::sinh(x); }, tnopflt, tnopdbl);
    bench(
            "tanh", [](auto x) noexcept { return fvm::tanh(x); },
            [](auto x) noexcept { return std::tanh(x); }, tnopflt, tnopdbl);
    bench(
            "cos_fast", [](auto x) noexcept { return fvm::cos_fast(x); },
            [](auto x) noexcept { return std::cos(x); }, tnopflt, tnopdbl);
    bench(
            "sin_fast", [](auto x) noexcept { return fvm::sin_fast(x); },
            [](auto x) noexcept { return std::sin(x); }, tnopflt, tnopdbl);
    bench(
            "tan_fast", [](auto x) noexcept { return fvm::tan_fast(x); },
            [](auto x) noexcept { return std::tan(x); }, tnopflt, tnopdbl);
    bench(
            "acos", [](auto x) noexcept { return fvm::acos(x); },
            [](auto x) noexcept { return std::acos(x); }, tnopflt, tnopdbl);
    bench(
            "asin", [](auto x) noexcept { return fvm::asin(x); },
            [](auto x) noexcept { return std::asin(x); }, tnopflt, tnopdbl);
    bench(
            "atan", [](auto x) noexcept { return fvm::atan(x); },
            [](auto x) noexcept { return std::atan(x); }, tnopflt, tnopdbl);
    bench(
            "sqrt", [](auto x) noexcept { return fvm::sqrt(x); },
            [](auto x) noexcept { return std::sqrt(x); }, tnopflt, tnopdbl);
    bench(
            "cbrt", [](auto x) noexcept { return fvm::cbrt(x); },
            [](auto x) noexcept { return std::cbrt(x); }, tnopflt, tnopdbl);
    bench(
            "fifthrt", [](auto x) noexcept { return fvm::fifthrt(x); },
            [](auto x) noexcept { return ref::fifthrt(x); }, tnopflt,
            tnopdbl);
    bench(
            "invsqrt", [](auto x) noexcept { return fvm::invsqrt(x); },
            [](auto x) noexcept { return ref::invsqrt(x); }, tnopflt,
            tnopdbl);
    bench(
            "invcbrt", [](auto x) noexcept { return fvm::invcbrt(x); },
            [](auto x) noexcept { return ref::invcbrt(x); }, tnopflt,
            tnopdbl);
    bench(
            "invfifthrt", [](auto x) noexcept { return fvm::invfifthrt(x); },
            [](auto x) noexcept { return ref::invfifthrt(x); }, tnopflt,
            tnopdbl);
}

int main()
{
    bench();

    return 0;
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
