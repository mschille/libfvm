/** @file tests/ulpdistance.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <algorithm>
#include <cassert>
#include <cmath>
#include <limits>

template <typename T>
T ulpdistance(const T from, const T to) noexcept
{
    if (std::isfinite(to)) {
        // determine ULP in terms of to; three cases:
        // 1. abs(to) is the maximum finite value of T, in which case we use
        //    the value immediately before it to get the ULP,
        // 2. abs(to) is normal, but not as in case 1, then we use the value
        //    immediately after it to get the ULP
        // 3. abs(to) is subnormal, in which case the ULP is T's minimum
        //    subnormal value
        const T ulp = std::max(
                std::nextafter(T(0), std::numeric_limits<T>::infinity()),
                (std::abs(to) != std::numeric_limits<T>::max())
                        ? (std::nextafter(
                                   std::abs(to),
                                   std::numeric_limits<T>::infinity()) -
                           std::abs(to))
                        : (std::abs(to) -
                           std::nextafter(
                                   std::abs(to),
                                   -std::numeric_limits<T>::infinity())));
        if (std::isfinite(from)) {
            return (from - to) / ulp;
        } else if (std::isinf(from)) {
            // assume infinity and maximum finite value are right next to each
            // other, so use the maximum finite value, and add a correction
            // for that
            const T retVal =
                    (std::copysign(std::numeric_limits<T>::max(), from) -
                     to) /
                    ulp;
            return retVal + std::copysign(T(1), retVal);
        } else {
            assert(std::isnan(from));
            return std::numeric_limits<T>::quiet_NaN();
        }
    } else if (std::isinf(to)) {
        // take ULP from maximum finite value for calculations
        const T ulp = std::numeric_limits<T>::max() -
                      std::nextafter(std::numeric_limits<T>::max(),
                                     -std::numeric_limits<T>::infinity());
        if (std::isfinite(from)) {
            const T retVal =
                    (from -
                     std::copysign(std::numeric_limits<T>::max(), to)) /
                    ulp;
            return retVal - std::copysign(T(1), retVal);
        } else if (std::isinf(from)) {
            // either it's an infinity with the same sign, in which case the
            // corrections for using the maximum instead of the infinity
            // cancel, or the infinities have different sign, so we need not
            // bother with the correction...
            return (std::copysign(std::numeric_limits<T>::max(), from) -
                    std::copysign(std::numeric_limits<T>::max(), to)) /
                   ulp;
        } else {
            assert(std::isnan(from));
            return std::numeric_limits<T>::quiet_NaN();
        }
    } else {
        assert(std::isnan(to));
        return std::numeric_limits<T>::quiet_NaN();
    }
    // should never reach this point
    assert(false);
    return std::numeric_limits<T>::quiet_NaN();
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
