/** @file tests/c-runtime-info.c
 *
 * prints C and machine specific runtimetime parameters, e.g. sizeof(type) for
 * different types, determines the number of bits in mantissa and exponent of
 * floating point types (excluding sign bits)
 *
 * @author Manuel Schiller
 * @date 2005-08-07
 * @author Manuel Schiller
 * @date 2021-01-26
 *
 * For copyright and license information, see the end of the file.
 */
#include <stdio.h>
#include <limits.h>

#if defined(__GLIBC__)
#include <gnu/libc-version.h>
#endif /* __GLIBC__ */

/* the macro below expects type names to be one word */
typedef long double ldouble;

/* "polymorphic" floating point mantissa and exponent length discovery
 * functions - calling the macro instantiates a template */
#define polyfuncs(type)                                                      \
    static int mantbits_##type()                                             \
    {                                                                        \
        type one, incr, res;                                                 \
        int retVal;                                                          \
                                                                             \
        /* this should be obvious - we find the smallest number that gives   \
         * something greater than one when added to one */                   \
        one = incr = 1;                                                      \
        retVal = 0;                                                          \
        do {                                                                 \
            incr *= ((type) 1) / ((type) 2);                                 \
            ++retVal;                                                        \
            res = one + incr;                                                \
        } while (res - one > 0);                                             \
                                                                             \
        /* we bail out one iteration late - decrement retVal */              \
        return retVal - 1;                                                   \
    }                                                                        \
    static int expbits_##type()                                              \
    {                                                                        \
        type inf = ((type) 1) / ((type) 0), test;                            \
        int pos = 0, retVal;                                                 \
                                                                             \
        /* how many times can we multiply be two until we overflow? */       \
        for (test = (type) 1; test < inf; test *= (type) 2) ++pos;           \
        /* find base 2 logarithm of that number */                           \
        for (retVal = 1; (1 << retVal) < pos; ++retVal)                      \
            ;                                                                \
                                                                             \
        return retVal;                                                       \
    }

polyfuncs(float)
polyfuncs(double)
polyfuncs(ldouble)

static void endianness()
{
    // very ugly and messy code follows...
    union {
        unsigned u;
        char c[sizeof(unsigned) < 5 ? 5 : sizeof(unsigned)];
    } u;
    unsigned i;
    char c;
    for (i = 0; i < sizeof(u.c); ++i) u.c[i] = 0;
    // figure out bits per char (byte)
    for (c = 1, i = 0; c; ++i, c <<= 1)
        ;
    printf("A bytes has %u bits on this machine (CHAR_BIT is %d).\n", i, CHAR_BIT);
    // will read "UNIX" on a big endian system (assuming 8 bits/byte)
    u.u = (((unsigned) 'U') << (3 * i)) | (((unsigned) 'N') << (2 * i)) |
          (((unsigned) 'I') << i) | ((unsigned) 'X');
    printf("byte order check: %c%c%c%c - ", u.c[0], u.c[1], u.c[2], u.c[3]);
    if ('U' == u.c[0] && 'N' == u.c[1] && 'I' == u.c[2] && 'X' == u.c[3]) {
        printf("big endian.\n");
    } else if ('X' == u.c[0] && 'I' == u.c[1] && 'N' == u.c[2] &&
               'U' == u.c[3]) {
        printf("little endian.\n");
    } else if ('N' == u.c[0] && 'U' == u.c[1] && 'X' == u.c[2] &&
               'I' == u.c[3]) {
        printf("middle endian (PDP11).\n");
    } else {
        printf("unknown.\n");
    }
    printf("\n");
}

int main()
{
    /* say something about known C compilers */
#if defined(__clang__)
    printf("__clang__ defined, version %s\n", __clang_version__);
#endif /* __clang__ */
#if defined(__INTEL_COMPILER)
    printf("__INTEL_COMPILER defined, version %d\n", __INTEL_COMPILER);
#endif /* _INTEL_COMPILER */
#if defined(__SUNPRO_C)
    printf("__SUNPRO_C defined, version 0x%x\n", __SUNPRO_C);
#endif
#if defined(__TINYC__)
    printf("__TINYC__ defined, version %d\n", __TINYC__);
#endif /* __TINYC__ */
#if defined(__GNUC__)
    printf("__GNUC__ defined, version %d.%d.%d\n", __GNUC__, __GNUC_MINOR__,
           __GNUC_PATCHLEVEL__);
#endif /* __GNUC__ */

    /* say something about the libc version, if we recognise it */
#if defined(__GLIBC__)
    printf("glibc version: %s\n", gnu_get_libc_version());
#endif /* __GLIBC__ */

    /* print a couple of machine parameters */
    endianness();

    /* sizes of basic types */
    printf("char\t\tis %2zd bytes long.\n", sizeof(char));
    printf("short\t\tis %2zd bytes long.\n", sizeof(short));
    printf("int\t\tis %2zd bytes long.\n", sizeof(int));
    printf("long\t\tis %2zd bytes long.\n", sizeof(long));
    printf("long long\tis %2zd bytes long.\n\n", sizeof(long long));
    printf("float\t\tis %2zd bytes long.\n", sizeof(float));
    printf("double\t\tis %2zd bytes long.\n", sizeof(double));
    printf("long double\tis %2zd bytes long.\n\n", sizeof(long double));
    printf("void *\t\tis %2zd bytes long.\n\n", sizeof(void*));

    printf("float\t\thas %d mantissa bits.\n", mantbits_float());
    printf("float\t\thas %d exponent bits.\n", expbits_float());
    printf("double\t\thas %d mantissa bits.\n", mantbits_double());
    printf("double\t\thas %d exponent bits.\n", expbits_double());
    printf("long double\thas %d mantissa bits.\n", mantbits_ldouble());
    printf("long double\thas %d exponent bits.\n", expbits_ldouble());

    return 0;
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

/* vim: sw=4:tw=78:ft=c:et */
