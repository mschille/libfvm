/** @file tests/cxx-runtime-info.cc
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2021-01-26
 *
 * For copyright and license information, see the end of the file.
 */
#include <cstdio>
#include <cxxabi.h>

#if defined(__GLIBC__)
#include <gnu/libc-version.h>
#endif /* __GLIBC__ */

int main()
{
    // say something about known C++ compilers
#if defined(__clang__)
    printf("__clang__ defined, version %s\n", __clang_version__);
#endif // __clang__
#if defined(__INTEL_COMPILER)
    printf("__INTEL_COMPILER defined, version %d\n", __INTEL_COMPILER);
#endif // _INTEL_COMPILER
#if defined(__SUNPRO_CC)
    printf("__SUNPRO_CC defined, version 0x%x\n", __SUNPRO_CC);
#endif // __SUNPRO_CC
#if defined(__GNUC__)
    printf("__GNUC__ defined, version %d.%d.%d\n", __GNUC__, __GNUC_MINOR__,
           __GNUC_PATCHLEVEL__);
#endif // __GNUC__

    // say something about the libc version, if we recognise it
#if defined(__GLIBC__)
    printf("glibc version: %s\n", gnu_get_libc_version());
#endif // __GLIBC__

    // C++ version info
    std::printf("__cplusplus is %ld\n", __cplusplus);

    // very old version of libstdc++
#ifdef __GLIBCPP__
    std::printf("GLIBCPP: %d\n", __GLIBCPP__);
#endif
    // more recent version of libstdc++
#ifdef __GLIBCXX__
#ifdef _GLIBCXX_RELEASE
    std::printf("GLIBCXX release %d, date code %d\n", _GLIBCXX_RELEASE, __GLIBCXX__);
#else
    std::printf("GLIBCXX date code %d\n", __GLIBCXX__);
#endif
#endif
    // libc++ version
#ifdef _LIBCPP_VERSION
    std::printf("libc++ version %d, libc++abi version %d\n", _LIBCPP_VERSION, _LIBCPPABI_VERSION);
#endif
    return 0;
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
