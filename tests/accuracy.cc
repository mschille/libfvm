/** @file tests/accuracy.cc
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-07-22
 *
 * For copyright and license information, see the end of the file.
 */
#include <cmath>
#include <cstdio>
#include <limits>
#include <random>
#include <tuple>

#include "ulpdistance.h"

#include "libfvm/acos.h"
#include "libfvm/asin.h"
#include "libfvm/atan.h"
#include "libfvm/cbrt.h"
#include "libfvm/cos.h"
#include "libfvm/cosh.h"
#include "libfvm/exp.h"
#include "libfvm/fifthrt.h"
#include "libfvm/invcbrt.h"
#include "libfvm/invfifthrt.h"
#include "libfvm/invsqrt.h"
#include "libfvm/log.h"
#include "libfvm/sin.h"
#include "libfvm/sinh.h"
#include "libfvm/sqrt.h"
#include "libfvm/tan.h"
#include "libfvm/tanh.h"

static std::mt19937_64 rng;

template <typename T>
T uniformrng(const T min, const T max) noexcept
{
    const double f = double(rng()) / double(rng.max());
    return T(f * min + (1. - f) * max);
}

// scale invariant RNG - flat in exponent range, and flat in allowed range of
// mantissa for given exponent
template <typename T>
T scaleinvrng(const T min, const T max) noexcept
{
    using U = typename std::mt19937_64::result_type;
    using I = typename std::make_signed<U>::type;
    static_assert(std::is_unsigned<U>::value, "U is expected to be unsigned");
    int minexp, maxexp;
    std::frexp(min, &minexp);
    std::frexp(max, &maxexp);
    bool mixedsign = false;
    if (std::copysign(T(1), min) * std::copysign(T(1), max) < 0) {
        maxexp = std::max(minexp, maxexp);
        minexp = std::numeric_limits<T>::min_exponent - 1;
        mixedsign = true;
    }
    T retVal = 0;
    do {
        // generate exponent
        const int exp = uniformrng(minexp, 1 + maxexp);
        // generate random number
        U tmprnd = rng();
        // strip of sign, if needed
        const bool sign = (tmprnd & 1) && mixedsign;
        tmprnd >>= 1;
        // generate mantissa from remainder
        retVal = 0.75 + 0.25 * double(I(tmprnd) - I(rng.max() >> 2)) /
                                double(rng.max() >> 2);
        // assemble floating point number
        retVal = std::ldexp((1 - 2 * sign) * retVal, exp);
    } while (retVal < min || retVal > max);
    return retVal;
}

template <bool SCALEINVRNG = true, typename T, typename FN, typename FNREF>
std::tuple<T, T, T> check_ulp(const char* name, T min, T max, FN fn,
                              FNREF fnref)
{
    constexpr unsigned ntrials = 1 << 20;
    T ulpmin = std::numeric_limits<T>::max(),
      ulpmax = -std::numeric_limits<T>::max();
    double ulpavg = 0;
    unsigned j = 0;
    for (unsigned i = 0; ntrials != i; ++i) {
        const T x = (i & 1) ? uniformrng(min, max) : scaleinvrng(min, max);
        const T y = fn(x), yref = fnref(x);
        const T ulpdiff = ulpdistance(y, yref);
        if (std::isfinite(ulpdiff)) {
            if (ulpdiff < ulpmin) ulpmin = ulpdiff;
            if (ulpdiff > ulpmax) ulpmax = ulpdiff;
            if (std::abs(ulpdiff) < T(65536)) {
                ulpavg = (j * ulpavg / (j + 1)) + ulpdiff * ulpdiff / (j + 1);
                ++j;
            } else {
                // std::printf("[DEBUG]: x % 24.16e y % 24.16e yref
                // % 24.16e\n", x, y, yref);
            }
        } else {
            std::printf("[DEBUG]: NaN from x %e y %e yref %e ulpdiff %e\n", x,
                        y, yref, ulpdiff);
        }
    }
    ulpavg = std::sqrt(ulpavg);
    std::printf("%20s: on %9u/%9u points: within % e and % e ULP of "
                "reference, RMS % e ULP",
                name, j, ntrials, ulpmin, ulpmax, ulpavg);
    return std::make_tuple(ulpmin, ulpmax, ulpavg);
}

template <typename T>
struct test_table {
    const char* name;
    const T xmin, xmax;
    const T lim_ulpmin, lim_ulpmax, lim_ulprms;
    T (*fn)(T);
    T (*fnref)(T);
};

namespace ref {
    template <typename T>
    T id(const T x) noexcept
    {
        return x;
    }
    template <typename T>
    T inv(const T x) noexcept
    {
        return T(1) / x;
    }
} // namespace ref

namespace adapt {
    template <typename T>
    T sqrt(const T x) noexcept
    {
        const T tmp = fvm::sqrt(x);
        return tmp * tmp;
    }
    template <typename T>
    T cbrt(const T x) noexcept
    {
        const T tmp = fvm::cbrt(x);
        return tmp * tmp * tmp;
    }
    template <typename T>
    T fifthrt(const T x) noexcept
    {
        const T tmp = fvm::fifthrt(x);
        return (tmp * tmp) * (tmp * tmp) * tmp;
    }
    template <typename T>
    T invsqrt(const T x) noexcept
    {
        const T tmp = fvm::invsqrt(x);
        return tmp * tmp;
    }
    template <typename T>
    T invcbrt(const T x) noexcept
    {
        const T tmp = fvm::invcbrt(x);
        return tmp * tmp * tmp;
    }
    template <typename T>
    T invfifthrt(const T x) noexcept
    {
        const T tmp = fvm::invfifthrt(x);
        return (tmp * tmp) * (tmp * tmp) * tmp;
    }
} // namespace adapt

static const test_table<float> tbl_flt[] = {
        {"exp", -87.33655f, 88.72284f, -2.f, 2.f, 0.26f,
         static_cast<float (*)(float)>(fvm::exp),
         static_cast<float (*)(float)>(std::exp)},
        {"log", std::numeric_limits<float>::min(),
         std::numeric_limits<float>::max(), -3.f, 3.f, 0.09f,
         static_cast<float (*)(float)>(fvm::log),
         static_cast<float (*)(float)>(std::log)},
        {"cosh", -89.4160f, 89.4160f, -5.f, 5.f, 0.68f,
         static_cast<float (*)(float)>(fvm::cosh),
         static_cast<float (*)(float)>(std::cosh)},
        {"sinh", -89.4160f, 89.4160f, -5.f, 5.f, 0.75f,
         static_cast<float (*)(float)>(fvm::sinh),
         static_cast<float (*)(float)>(std::sinh)},
        {"tanh", -8.31776619f, 8.31776619f, -5.f, 5.f, 0.77f,
         static_cast<float (*)(float)>(fvm::tanh),
         static_cast<float (*)(float)>(std::tanh)},
        {"cos_fast", -3.141592741f, 3.141592741f, -3.f, 4.f, 0.51f,
         static_cast<float (*)(float)>(fvm::cos_fast),
         static_cast<float (*)(float)>(std::cos)},
        {"sin_fast", -3.141592741f, 3.141592741f, -3.f, 4.f, 0.57f,
         static_cast<float (*)(float)>(fvm::sin_fast),
         static_cast<float (*)(float)>(std::sin)},
        {"tan_fast", -3.141592741f, 3.141592741f, -5.f, 5.f, 0.76f,
         static_cast<float (*)(float)>(fvm::tan_fast),
         static_cast<float (*)(float)>(std::tan)},
        {"acos", -1.f, 1.f, -5.f, 7.f, 1.29f,
         static_cast<float (*)(float)>(fvm::acos),
         static_cast<float (*)(float)>(std::acos)},
        {"asin", -1.f, 1.f, -7.f, 7.f, 1.03f,
         static_cast<float (*)(float)>(fvm::asin),
         static_cast<float (*)(float)>(std::asin)},
        {"atan", -std::numeric_limits<float>::max(),
         std::numeric_limits<float>::max(), -6.f, 5.f, 0.93f,
         static_cast<float (*)(float)>(fvm::atan),
         static_cast<float (*)(float)>(std::atan)},
        {"sqrt", 0.f, std::numeric_limits<float>::max(), -3.f, 3.f, 0.89f,
         static_cast<float (*)(float)>(adapt::sqrt),
         static_cast<float (*)(float)>(ref::id)},
        {"cbrt", 0.f, std::numeric_limits<float>::max(), -4.f, 4.f, 1.08f,
         static_cast<float (*)(float)>(adapt::cbrt),
         static_cast<float (*)(float)>(ref::id)},
        {"fifthrt", 0.f, std::numeric_limits<float>::max(), -8.f, 8.f, 1.94f,
         static_cast<float (*)(float)>(adapt::fifthrt),
         static_cast<float (*)(float)>(ref::id)},
        {"invsqrt", std::numeric_limits<float>::min(),
         std::numeric_limits<float>::max(), -4.f, 4.f, 0.81f,
         static_cast<float (*)(float)>(adapt::invsqrt),
         static_cast<float (*)(float)>(ref::inv)},
        {"invcbrt", std::numeric_limits<float>::min(),
         std::numeric_limits<float>::max(), -5.f, 5.f, 0.97f,
         static_cast<float (*)(float)>(adapt::invcbrt),
         static_cast<float (*)(float)>(ref::inv)},
        {"invfifthrt", std::numeric_limits<float>::min(),
         std::numeric_limits<float>::max(), -9.f, 9.f, 1.50f,
         static_cast<float (*)(float)>(adapt::invfifthrt),
         static_cast<float (*)(float)>(ref::inv)},
};
static const test_table<double> tbl_dbl[] = {
        {"exp", -708.39641853226408, 709.78271289338397, -3., 2., 0.27,
         static_cast<double (*)(double)>(fvm::exp),
         static_cast<double (*)(double)>(std::exp)},
        {"log", std::numeric_limits<double>::min(),
         std::numeric_limits<double>::max(), -4., 5., 0.04,
         static_cast<double (*)(double)>(fvm::log),
         static_cast<double (*)(double)>(std::log)},
        {"cosh", -710.4758600739438634, 710.4758600739438634, -6., 5., 0.68,
         static_cast<double (*)(double)>(fvm::cosh),
         static_cast<double (*)(double)>(std::cosh)},
        {"sinh", -710.4758600739438634, 710.4758600739438634, -8., 6., 0.74,
         static_cast<double (*)(double)>(fvm::sinh),
         static_cast<double (*)(double)>(std::sinh)},
        {"tanh", -1.83684002848385508067e1, 1.83684002848385508067e1, -7., 7.,
         0.48, static_cast<double (*)(double)>(fvm::tanh),
         static_cast<double (*)(double)>(std::tanh)},
        {"cos_fast", -3.141592653589793116, 3.141592653589793116, -3., 3.,
         0.51, static_cast<double (*)(double)>(fvm::cos_fast),
         static_cast<double (*)(double)>(std::cos)},
        {"sin_fast", -3.141592653589793116, 3.141592653589793116, -3., 3.,
         0.54, static_cast<double (*)(double)>(fvm::sin_fast),
         static_cast<double (*)(double)>(std::sin)},
        {"tan_fast", -3.141592653589793116, 3.141592653589793116, -5., 5.,
         0.72, static_cast<double (*)(double)>(fvm::tan_fast),
         static_cast<double (*)(double)>(std::tan)},
        {"acos", -1., 1., -7., 7., 1.62,
         static_cast<double (*)(double)>(fvm::acos),
         static_cast<double (*)(double)>(std::acos)},
        {"asin", -1., 1., -7., 6., 0.81,
         static_cast<double (*)(double)>(fvm::asin),
         static_cast<double (*)(double)>(std::asin)},
        {"atan", -std::numeric_limits<double>::max(),
         std::numeric_limits<double>::max(), -5., 6., 0.17,
         static_cast<double (*)(double)>(fvm::atan),
         static_cast<double (*)(double)>(std::atan)},
        {"sqrt", 0., std::numeric_limits<double>::max(), -3., 3., 0.89,
         static_cast<double (*)(double)>(adapt::sqrt),
         static_cast<double (*)(double)>(ref::id)},
        {"cbrt", 0., std::numeric_limits<double>::max(), -4., 4., 1.10,
         static_cast<double (*)(double)>(adapt::cbrt),
         static_cast<double (*)(double)>(ref::id)},
        {"fifthrt", 0., std::numeric_limits<double>::max(), -8., 8., 1.88,
         static_cast<double (*)(double)>(adapt::fifthrt),
         static_cast<double (*)(double)>(ref::id)},
        {"invsqrt", std::numeric_limits<double>::min(),
         std::numeric_limits<double>::max(), -4., 4., 0.82,
         static_cast<double (*)(double)>(adapt::invsqrt),
         static_cast<double (*)(double)>(ref::inv)},
        {"invcbrt", std::numeric_limits<double>::min(),
         std::numeric_limits<double>::max(), -5., 5., 0.96,
         static_cast<double (*)(double)>(adapt::invcbrt),
         static_cast<double (*)(double)>(ref::inv)},
        {"invfifthrt", std::numeric_limits<double>::min(),
         std::numeric_limits<double>::max(), -9., 9., 1.59,
         static_cast<double (*)(double)>(adapt::invfifthrt),
         static_cast<double (*)(double)>(ref::inv)},
};

template <typename T, std::size_t N>
std::tuple<unsigned, unsigned> check_ulp(const test_table<T> (&tbl)[N])
{
    unsigned fail = 0, tot = 0;
    const char* datatype = std::is_same<T, float>::value ? "float" : "double";
    std::printf("%s(%s): begin tests\n", __func__, datatype);
    for (const auto& t : tbl) {
        const auto tmp = check_ulp(t.name, t.xmin, t.xmax, t.fn, t.fnref);
        const bool failed = (std::get<0>(tmp) < t.lim_ulpmin ||
                             std::get<1>(tmp) > t.lim_ulpmax ||
                             std::get<2>(tmp) > t.lim_ulprms);
        std::printf(" - %s\n", (failed ? "FAILED" : "OKAY"));
        if (failed) ++fail;
        ++tot;
    }
    if (fail) {
        std::printf("%s(%s): %u/%u tests failed\n", __func__, datatype, fail,
                    tot);
    } else {
        std::printf("%s(%s): all %u tests successful\n", __func__, datatype,
                    tot);
    }
    return std::make_tuple(fail, tot);
}

int main(int /* unused */, char* argv[])
{
    unsigned fail = 0, tot = 0;
    const auto result_flt = check_ulp(tbl_flt);
    fail += std::get<0>(result_flt), tot += std::get<1>(result_flt);
    const auto result_dbl = check_ulp(tbl_dbl);
    fail += std::get<0>(result_dbl), tot += std::get<1>(result_dbl);
    if (fail) {
        std::printf("%s: %u/%u tests FAILED.\n", argv[0], fail, tot);
    } else {
        std::printf("%s: All %u test successful.\n", argv[0], tot);
    }

    return (fail > 0);
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
