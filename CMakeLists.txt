cmake_minimum_required(VERSION 3.1 FATAL_ERROR)
project(libfvm VERSION 0.1.0 LANGUAGES C CXX)

# do not override host project's compilation if libfvm is not the top-level
if (CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
    cmake_policy(VERSION 3.1) # Needed for Mac
    set(CMAKE_C_EXTENSIONS OFF)
    set(CMAKE_C_STANDARD_REQUIRED ON)
    set(CMAKE_CXX_EXTENSIONS OFF)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)
    set(libfvm_arch_flags "-march=native")
else()
    set(libfvm_arch_flags "")
endif()

add_library(libfvm INTERFACE)
set_target_properties(libfvm PROPERTIES INTERFACE_CXX_STANDARD 14 INTERFACE_C_STANDARD 11)
# CMAKE > 3.8
if (NOT (${CMAKE_VERSION} VERSION_LESS "3.8.0"))
    target_compile_features(libfvm INTERFACE cxx_std_14 c_std_11)
else()
    # older CMake versions don't know about cxx_std_14 and the like, they know
    # about the individual language features instead
    target_compile_features(libfvm INTERFACE
	# C++ 98
	cxx_template_template_parameters
	# C++ 11
	cxx_alias_templates cxx_alignas cxx_alignof cxx_attributes cxx_auto_type
	cxx_constexpr cxx_decltype_incomplete_return_types cxx_decltype
	cxx_default_function_template_args cxx_defaulted_functions
	cxx_defaulted_move_initializers cxx_delegating_constructors
	cxx_deleted_functions cxx_enum_forward_declarations
	cxx_explicit_conversions cxx_extended_friend_declarations
	cxx_extern_templates cxx_final cxx_func_identifier
	cxx_generalized_initializers cxx_inheriting_constructors
	cxx_inline_namespaces cxx_lambdas cxx_local_type_template_args
	cxx_long_long_type cxx_noexcept cxx_nonstatic_member_init cxx_nullptr
	cxx_override cxx_range_for cxx_raw_string_literals
	cxx_reference_qualified_functions cxx_right_angle_brackets
	cxx_rvalue_references cxx_sizeof_member cxx_static_assert cxx_strong_enums
	cxx_thread_local cxx_trailing_return_types cxx_unicode_literals
	cxx_uniform_initialization cxx_unrestricted_unions cxx_user_literals
	cxx_variadic_macros cxx_variadic_templates
	# C++ 14
	cxx_aggregate_default_initializers cxx_attribute_deprecated
	cxx_binary_literals cxx_contextual_conversions cxx_decltype_auto
	cxx_digit_separators cxx_generic_lambdas cxx_lambda_init_captures
	cxx_relaxed_constexpr cxx_return_type_deduction cxx_variable_templates)
endif()
target_compile_options(libfvm INTERFACE
    -Wpedantic -Wall -Wextra -ftree-vectorize ${libfvm_arch_flags})
target_include_directories(libfvm INTERFACE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include> $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>
    $<INSTALL_INTERFACE:include>)

configure_file(include/libfvm/fvm-config.h.in include/libfvm/fvm-config.h)

if (NOT CMAKE_BUILD_TYPE)
  message(STATUS "No build type selected, default to Release")
  set(CMAKE_BUILD_TYPE "Release") # -O3 -DNDEBUG
endif()

if(${CMAKE_GENERATOR} STREQUAL "Ninja")
  add_compile_options(-fdiagnostics-color)
endif()

option(libfvm_disable_tests "disable unit tests" OFF)
if (NOT libfvm_disable_tests)
    enable_testing()
    add_subdirectory(tests)
endif()

set(libfvm_header_destination ${CMAKE_INSTALL_PREFIX}/include CACHE STRING
    "destination for libfvm headers, i.e. headers can be found in ${libfvm_header_destination}/libfvm")

install(TARGETS libfvm DESTINATION lib EXPORT libfvmTargets)
install(DIRECTORY include/libfvm DESTINATION ${libfvm_header_destination}
    FILES_MATCHING PATTERN "*.h")
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/include/libfvm/fvm-config.h
    DESTINATION ${libfvm_header_destination}/libfvm)
install(EXPORT libfvmTargets FILE libfvmTargets.cmake
    DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/cmake/libfvm)
install(FILES README.md LICENSE
    DESTINATION ${CMAKE_INSTALL_PREFIX}/share/doc/libfvm)

include(CMakePackageConfigHelpers)
# generate the config file that is includes the exports
configure_package_config_file(${CMAKE_CURRENT_SOURCE_DIR}/Config.cmake.in
    "${CMAKE_CURRENT_BINARY_DIR}/libfvmConfig.cmake"
    INSTALL_DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/example"
    NO_SET_AND_CHECK_MACRO
    NO_CHECK_REQUIRED_COMPONENTS_MACRO
    )
# generate the version file for the config file
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/libfvmConfigVersion.cmake" VERSION
    "${libfvm_VERSION_MAJOR}.${libfvm_VERSION_MINOR}.${libfvm_VERSION_PATCH}"
    COMPATIBILITY AnyNewerVersion)

# install the configuration file
install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/libfvmConfig.cmake
    DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/cmake/libfvm)
export(EXPORT libfvmTargets
      FILE "${CMAKE_CURRENT_BINARY_DIR}/libfvmTargets.cmake")

SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "Manuel.Schiller@cern.ch") #required
include(InstallRequiredSystemLibraries)
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
set(CPACK_PACKAGE_VERSION_MAJOR "${libfvm_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${libfvm_VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${libfvm_VERSION_PATCH}")
INCLUDE(CPack)

# Copyright (C) CERN for the benefit of the LHCb collaboration
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization
# or submit itself to any jurisdiction.
